package com.android.aircountr;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.aircountr.adapters.InvoiceListAdapter;
import com.android.aircountr.constants.CreateUrl;
import com.android.aircountr.constants.UrlConstants;
import com.android.aircountr.objects.InvoiceListItem;
import com.android.aircountr.prefrences.AppPreferences;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by gaura on 6/2/2016.
 */
public class InvoiceListActivity extends BaseActivity implements UrlConstants {

    private String TAG = this.getClass().getSimpleName();
    private ImageView iv_backBtn;
    private TextView tv_selectedDate;
    private TextView tv_amount;
    private TextView tv_vendorName;
    private TextView tv_categoryName;
    private ListView lv_invoiceList;
    private TextView tv_msgText;
    private String strTimeStamp = "", strDay = "";
    private ArrayList<InvoiceListItem> invoiceDataList;
    private InvoiceListItem rowData;
    private InvoiceListAdapter mInvoiceListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_list);
        init();

        Intent intent = getIntent();
        strTimeStamp = intent.getStringExtra(TIMESTAMP);
        strDay = intent.getStringExtra(DAY_OF_MONTH);

        iv_backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InvoiceListActivity.this.finish();
            }
        });

        invoiceDataList = new ArrayList<>();
        mInvoiceListAdapter = new InvoiceListAdapter(InvoiceListActivity.this, invoiceDataList);
        lv_invoiceList.setAdapter(mInvoiceListAdapter);
        mInvoiceListAdapter.onDataSetChanged(invoiceDataList);

        if (strTimeStamp != null && !strTimeStamp.equals("") && strDay != null && !strDay.equals(""))
            sendInvoiceListRequest(strTimeStamp, strDay);
        else showDialog("Alert", "oh! something went wrong", "OK");
    }

    private void init() {
        iv_backBtn = (ImageView) findViewById(R.id.iv_backBtn);
        tv_selectedDate = (TextView) findViewById(R.id.tv_selectedDate);
        tv_amount = (TextView) findViewById(R.id.tv_amount);
        tv_vendorName = (TextView) findViewById(R.id.tv_vendorName);
        tv_categoryName = (TextView) findViewById(R.id.tv_categoryName);
        lv_invoiceList = (ListView) findViewById(R.id.lv_invoiceList);
        tv_msgText = (TextView) findViewById(R.id.tv_msgText);

        tv_selectedDate.setTypeface(SEMIBOLD);
        tv_amount.setTypeface(REGULAR);
        tv_vendorName.setTypeface(REGULAR);
        tv_categoryName.setTypeface(REGULAR);
        tv_msgText.setTypeface(SEMIBOLD);
    }

    private void sendInvoiceListRequest(String timeStamp, String day) {
        if (!AppPreferences.getMerchantId(InvoiceListActivity.this).equals("") && !AppPreferences.getAccessToken(InvoiceListActivity.this).equals("")) {
            if (isNetworkAvailable()) {
                new InvoiceListAsync().execute(timeStamp, day);
            } else
                showDialog("Error", getResources().getString(R.string.no_internet), "OK");
        } else {
            showDialog("Alert", "You seems to be logged out", "OK");
        }
    }

    private class InvoiceListAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading("Plz Wait...", false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Log.d(TAG, "Response : " + s);
            invoiceDataList.clear();
            if (s != null) {
                try {
                    JSONObject response = new JSONObject(s);
                    boolean success = response.getBoolean("success");
                    if (success) {
                        JSONArray invoiceArray = response.getJSONArray("invoice");
                        if (invoiceArray != null && invoiceArray.length() > 0) {
                            for (int i = 0; i < invoiceArray.length(); i++) {
                                JSONObject invoiceObj = invoiceArray.getJSONObject(i);
                                rowData = new InvoiceListItem();
                                String _vendorId = invoiceObj.getString("vendorId");
                                String _invoiceAmount = invoiceObj.getString("invoiceamount");
                                boolean _isAutoMode = invoiceObj.getBoolean("isautomode");
                                String _imageUrl = invoiceObj.optString("imageurl");
                                String _comments = invoiceObj.getString("comments");
                                String _vendorName = invoiceObj.getString("vendorname");
                                String _categoryName = invoiceObj.getString("categoryname");

                                rowData.setVendorId(_vendorId);
                                rowData.setInvoiceAmount(_invoiceAmount);
                                rowData.setAutoMode(_isAutoMode);
                                rowData.setComment(_comments);
                                rowData.setVendorName(_vendorName);
                                rowData.setCategoryName(_categoryName);
                                rowData.setImageUrl(_imageUrl);

                                invoiceDataList.add(rowData);
                            }
                            tv_msgText.setVisibility(View.GONE);
                            mInvoiceListAdapter.onDataSetChanged(invoiceDataList);
                        } else {
                            tv_msgText.setVisibility(View.VISIBLE);
                            tv_msgText.setText("No invoice found");
                        }
                    } else {
                        String msg = response.getString("msg");
                        tv_msgText.setVisibility(View.VISIBLE);
                        tv_msgText.setText(msg);
                        showDialog("Alert", msg, "OK");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                tv_msgText.setVisibility(View.VISIBLE);
                tv_msgText.setText("Sorry! found nothing");
            }
            hideLoading();
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.invoiceListUrl(AppPreferences.getMerchantId(InvoiceListActivity.this), params[0], params[1]);
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Authorization", AppPreferences.getAccessToken(InvoiceListActivity.this));
            try {
                HttpResponse response = httpClient.execute(httpGet);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return _response;
        }
    }
}
