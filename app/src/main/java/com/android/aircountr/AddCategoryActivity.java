package com.android.aircountr;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.aircountr.adapters.AddCategoryIconsListAdapter;
import com.android.aircountr.constants.CreateUrl;
import com.android.aircountr.objects.CategoryIconsListItem;
import com.android.aircountr.prefrences.AppPreferences;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


/**
 * Created by gaurav on 4/26/2016.
 */
public class AddCategoryActivity extends BaseActivity {

    private String TAG = this.getClass().getSimpleName();
    private ImageView iv_backBtn;
    private TextView tv_pageTitle;
    private EditText et_categoryName;
    private TextView tv_errCategoryName;
    private TextView tv_selectCategoryImgText;
    private GridView gv_categoryIconList;
    private TextView tv_saveBtn;
    private AddCategoryIconsListAdapter mAdapter;
    private ArrayList<CategoryIconsListItem> iconsList;
    private int iconResourceId = 0;
    private CategoryIconsListItem rowData;
    private int iconsResourceIds[] = {R.drawable.icon_groceries, R.drawable.icon_meat, R.drawable.icon_retail, R.drawable.icon_cylinder, R.drawable.icon_coal, R.drawable.icon_alchol, R.drawable.icon_dairy, R.drawable.icon_transportation};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);

        iv_backBtn = (ImageView) findViewById(R.id.iv_backBtn);
        tv_pageTitle = (TextView) findViewById(R.id.tv_pageTitle);
        et_categoryName = (EditText) findViewById(R.id.et_categoryName);
        tv_errCategoryName = (TextView) findViewById(R.id.tv_errCategoryName);
        tv_selectCategoryImgText = (TextView) findViewById(R.id.tv_selectCategoryImgText);
        gv_categoryIconList = (GridView) findViewById(R.id.gv_categoryIconList);
        tv_saveBtn = (TextView) findViewById(R.id.tv_saveBtn);

        tv_pageTitle.setTypeface(SEMIBOLD);
        et_categoryName.setTypeface(SEMIBOLD);
        tv_errCategoryName.setTypeface(REGULAR);
        tv_selectCategoryImgText.setTypeface(REGULAR);
        tv_saveBtn.setTypeface(SEMIBOLD);

        iconsList = new ArrayList<>();
        mAdapter = new AddCategoryIconsListAdapter(AddCategoryActivity.this, iconsList);
        gv_categoryIconList.setAdapter(mAdapter);
        for (int i = 0; i < iconsResourceIds.length; i++) {
            rowData = new CategoryIconsListItem();
            rowData.setResourceId(iconsResourceIds[i]);
            Log.d(TAG, "Resource Id : " + iconsResourceIds[i]);
            rowData.setIsSelected(false);
            iconsList.add(rowData);
        }
        mAdapter.onDataSetChanged(iconsList);

        gv_categoryIconList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                iconResourceId = iconsList.get(position).getResourceId();
                for (int i = 0; i < iconsList.size(); i++) {
                    iconsList.get(i).setIsSelected(false);
                }
                iconsList.get(position).setIsSelected(true);
                mAdapter.onDataSetChanged(iconsList);
            }
        });

        tv_saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidations()) {
                    sendAddCategoryRequest(et_categoryName.getText().toString().trim(), String.valueOf(iconResourceId));
                }
            }
        });

        iv_backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddCategoryActivity.this.finish();
            }
        });

        et_categoryName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString();
                if (s.length() > 0) {
                    tv_errCategoryName.setVisibility(View.GONE);
                } else {
                    tv_errCategoryName.setVisibility(View.VISIBLE);
                    tv_errCategoryName.setText("Plz enter category name");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private boolean checkValidations() {
        if (et_categoryName.getText().toString().trim().isEmpty()) {
            tv_errCategoryName.setVisibility(View.VISIBLE);
            tv_errCategoryName.setText("Plz enter category name");
            return false;
        } else if (iconResourceId == 0) {
            showDialog("Alert", "Plz select suitable category icon", "OK");
            return false;
        } else {
            return true;
        }
    }

    private void sendAddCategoryRequest(String categoryName, String resourceId) {
        if (AppPreferences.getMerchantId(AddCategoryActivity.this).equals("") && AppPreferences.getAccessToken(AddCategoryActivity.this).equals("")) {
            switchActivity(AddCategoryActivity.this, SignInActivity.class);
            AddCategoryActivity.this.finish();
        } else {
            if (isNetworkAvailable()) {
                new AddCategoryAsync().execute(AppPreferences.getAccessToken(AddCategoryActivity.this), categoryName, AppPreferences.getMerchantId(AddCategoryActivity.this), resourceId);
            } else {
                showDialog("Error", getResources().getString(R.string.no_internet), "OK");
            }
        }
    }

    private class AddCategoryAsync extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.addCategoriesUrl();
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(),
                    30000);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Authorization", params[0]);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", params[1]);
                jsonObject.put("merchantId", params[2]);
                jsonObject.put("resourceid", params[3]);
                httpPost.setEntity(new ByteArrayEntity(jsonObject.toString().getBytes("UTF8")));

                HttpResponse response = httpClient.execute(httpPost);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return _response;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading("Plz Wait...", false);
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                try {
                    JSONObject response = new JSONObject(result);
                    boolean success = response.getBoolean("success");
                    String message = response.getString("msg");
                    if (success) {
                        displayToast(message);
                        AddCategoryActivity.this.finish();
                    } else {
                        showDialog("Alert", message, "OK");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            hideLoading();
        }
    }
}
