package com.android.aircountr.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.aircountr.R;
import com.android.aircountr.objects.CategoryIconsListItem;
import com.android.aircountr.objects.CategoryListItem;

import java.util.ArrayList;

/**
 * Created by gaurav on 5/12/2016.
 */
public class CategorySpinnerAdapter extends BaseAdapter {

    private String TAG = this.getClass().getSimpleName();
    private ArrayList<CategoryListItem> mDataList;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    public Typeface REGULAR;

    public CategorySpinnerAdapter(Context mContext, ArrayList<CategoryListItem> mDataList) {
        this.mContext = mContext;
        this.mDataList = mDataList;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        REGULAR = Typeface.createFromAsset(mContext.getAssets(), "ProximaNova-Regular.ttf");
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mLayoutInflater.inflate(R.layout.row_drawer_list, null);
        ImageView itemIcon = (ImageView) convertView.findViewById(R.id.iv_itemIcon);
        TextView itemName = (TextView) convertView.findViewById(R.id.tv_itemName);
        itemName.setTypeface(REGULAR);

        itemIcon.setImageResource(Integer.valueOf(mDataList.get(position).getResourceId()));
        itemName.setText(mDataList.get(position).getCategoryName());
        return convertView;
    }
}
