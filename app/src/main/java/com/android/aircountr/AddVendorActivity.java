package com.android.aircountr;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.aircountr.constants.CreateUrl;
import com.android.aircountr.constants.UrlConstants;
import com.android.aircountr.prefrences.AppPreferences;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by architnf on 5/17/2016.
 */
public class AddVendorActivity extends BaseActivity implements UrlConstants {

    private String TAG = this.getClass().getSimpleName();
    private ImageView iv_backBtn;
    private TextView tv_pageTitle;
    private TextView tv_categoryName;
    private EditText et_vendorName;
    private TextView tv_errVendorName;
    private EditText et_vendorNumber;
    private TextView tv_errVendorNumber;
    private EditText et_vendorAddress;
    private TextView tv_errVendorAddress;
    private TextView tv_addVendorBtn;
    private String categoryId = "", categoryName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vendor);
        init();

        Intent intent = getIntent();
        categoryId = intent.getStringExtra(CATEGORY_ID);
        categoryName = intent.getStringExtra(CATEGORY_NAME);

        tv_categoryName.setText(categoryName);

        et_vendorName.addTextChangedListener(new GenericTextWatcher(et_vendorName));
        et_vendorNumber.addTextChangedListener(new GenericTextWatcher(et_vendorNumber));
        et_vendorAddress.addTextChangedListener(new GenericTextWatcher(et_vendorAddress));

        iv_backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddVendorActivity.this.finish();
            }
        });

        tv_addVendorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    sendAddVendorRequest(et_vendorName.getText().toString().trim(), et_vendorNumber.getText().toString().trim(), et_vendorAddress.getText().toString().trim());
                }
            }
        });
    }

    private void init() {
        iv_backBtn = (ImageView) findViewById(R.id.iv_backBtn);
        tv_pageTitle = (TextView) findViewById(R.id.tv_pageTitle);
        tv_categoryName = (TextView) findViewById(R.id.tv_categoryName);
        et_vendorName = (EditText) findViewById(R.id.et_vendorName);
        tv_errVendorName = (TextView) findViewById(R.id.tv_errVendorName);
        et_vendorNumber = (EditText) findViewById(R.id.et_vendorNumber);
        tv_errVendorNumber = (TextView) findViewById(R.id.tv_errVendorNumber);
        et_vendorAddress = (EditText) findViewById(R.id.et_vendorAddress);
        tv_errVendorAddress = (TextView) findViewById(R.id.tv_errVendorAddress);
        tv_addVendorBtn = (TextView) findViewById(R.id.tv_addVendorBtn);

        tv_pageTitle.setTypeface(SEMIBOLD);
        tv_categoryName.setTypeface(SEMIBOLD);
        et_vendorName.setTypeface(REGULAR);
        tv_errVendorName.setTypeface(REGULAR);
        et_vendorNumber.setTypeface(REGULAR);
        tv_errVendorNumber.setTypeface(REGULAR);
        et_vendorAddress.setTypeface(REGULAR);
        tv_errVendorAddress.setTypeface(REGULAR);
        tv_addVendorBtn.setTypeface(SEMIBOLD);
    }

    private boolean checkValidation() {
        if (et_vendorName.getText().toString().trim().isEmpty()) {
            tv_errVendorName.setVisibility(View.VISIBLE);
            tv_errVendorName.setText("Plz enter vendor name");
            return false;
        } else if (et_vendorNumber.getText().toString().trim().isEmpty()) {
            tv_errVendorNumber.setVisibility(View.VISIBLE);
            tv_errVendorNumber.setText("Plz enter vendor phone number");
            return false;
        } else if (et_vendorNumber.getText().toString().trim().length() < 10) {
            tv_errVendorNumber.setVisibility(View.VISIBLE);
            tv_errVendorNumber.setText("Plz enter 10 digits phone number");
            return false;
        } else if (et_vendorAddress.getText().toString().trim().isEmpty()) {
            tv_errVendorAddress.setVisibility(View.VISIBLE);
            tv_errVendorAddress.setText("Plz enter vendor address");
            return false;
        } else {
            tv_errVendorName.setVisibility(View.GONE);
            tv_errVendorNumber.setVisibility(View.GONE);
            tv_errVendorAddress.setVisibility(View.GONE);
            return true;
        }
    }

    private class GenericTextWatcher implements TextWatcher {

        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String text = s.toString();
            switch (view.getId()) {
                case R.id.et_vendorName:
                    if (text.length() > 0) {
                        tv_errVendorName.setVisibility(View.GONE);
                    } else {
                        tv_errVendorName.setVisibility(View.VISIBLE);
                        tv_errVendorName.setText("Plz enter vendor name");
                    }
                    break;
                case R.id.et_vendorNumber:
                    if (text.length() > 0)
                        if (text.length() != 10) {
                            tv_errVendorNumber.setVisibility(View.VISIBLE);
                            tv_errVendorNumber.setText("Plz enter 10 digits valid phone number");
                        } else
                            tv_errVendorNumber.setVisibility(View.GONE);
                    else {
                        tv_errVendorNumber.setVisibility(View.VISIBLE);
                        tv_errVendorNumber.setText("Plz enter phone number");
                    }
                    break;
                case R.id.et_vendorAddress:
                    if (text.length() > 0) {
                        tv_errVendorAddress.setVisibility(View.GONE);
                    } else {
                        tv_errVendorAddress.setVisibility(View.VISIBLE);
                        tv_errVendorAddress.setText("Plz enter vendor name");
                    }
                    break;
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    }

    private void sendAddVendorRequest(String vendorName, String vendorNumber, String vendorAddress) {
        if (!AppPreferences.getMerchantId(AddVendorActivity.this).equals("") && !AppPreferences.getAccessToken(AddVendorActivity.this).equals("")) {
            if (!categoryId.equals("") && !categoryName.equals("")) {
                if (isNetworkAvailable())
                    new AddVendorAsync().execute(vendorName, vendorNumber, vendorAddress);
                else
                    showDialog("Error", getResources().getString(R.string.no_internet), "OK");
            } else {
                showDialog("Alert", "oops! something is missing go back and try again", "OK");
            }
        } else {
            showDialog("Alert", "oops! you seems logged out", "OK");
        }
    }

    private class AddVendorAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading("Adding...", false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d(TAG,"Response :"+s);
            try {
                JSONObject response = new JSONObject(s);
                boolean success = response.getBoolean("success");
                String msg = response.getString("msg");
                if (success) {
                    displayToast(msg);
                    AddVendorActivity.this.finish();
                } else {
                    showDialog("Alert", msg, "OK");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            hideLoading();
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.addVendorUrl();
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Authorization", AppPreferences.getAccessToken(AddVendorActivity.this));
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", params[0]);
                jsonObject.put("number", params[1]);
                jsonObject.put("address", params[2]);
                jsonObject.put("merchantId", AppPreferences.getMerchantId(AddVendorActivity.this));
                jsonObject.put("category", categoryName);
                jsonObject.put("categoryId", categoryId);
                httpPost.setEntity(new ByteArrayEntity(jsonObject.toString().getBytes("UTF8")));

                HttpResponse response = httpClient.execute(httpPost);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return _response;
        }
    }
}
