package com.android.aircountr;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.aircountr.constants.CreateUrl;
import com.android.aircountr.prefrences.AppPreferences;
import com.android.aircountr.utils.CustomFonts;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.okhttp.internal.http.CacheRequest;
import com.squareup.okhttp.internal.spdy.Settings;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class SettingsActivity extends BaseActivity implements TimePickerDialog.OnTimeSetListener, View.OnClickListener {

    EditText bname, email,phone,hoursfrom,employees,hoursto;
    TextView bnameTitle, contactTitle, workTitle;
    ImageView back, editBname, editEmail, editOperationaHour, editEmployeeNO ;
    private String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    AlertDialog.Builder ab;
    private int BTN_TYPE = 0;
    KeyListener kl1,kl2, kl4,kl5,kl6;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_container);

        // Take the reference of each of the edit buttons
        back = (ImageView)findViewById(R.id.iv_backBtn);
        editBname = (ImageView)findViewById(R.id.edit_businesname);
        editEmail = (ImageView)findViewById(R.id.edit_email);
        editOperationaHour = (ImageView)findViewById(R.id.edit_operational_hour);
        editEmployeeNO = (ImageView)findViewById(R.id.edit_employee_no);

        /*make the EditText non editable and set Font style to PROXIMA_NOVA
        * also store the keyListener for each of the Edittext*/
        bname = (EditText)findViewById(R.id.setbname);
        kl1 = bname.getKeyListener();
        bname.setKeyListener(null);
        bname.setTypeface(CustomFonts.getTypeface(this,CustomFonts.PROXIMA_NOVA_REGULAR));
        email = (EditText)findViewById(R.id.setemail);
        kl2 = email.getKeyListener();
        email.setKeyListener(null);
        email.setTypeface(CustomFonts.getTypeface(this,CustomFonts.PROXIMA_NOVA_REGULAR));
        phone = (EditText)findViewById(R.id.setphone);
        phone.setKeyListener(null);
        phone.setTypeface(CustomFonts.getTypeface(this,CustomFonts.PROXIMA_NOVA_REGULAR));
        hoursfrom = (EditText)findViewById(R.id.sethourfrom);
        kl4 = hoursfrom.getKeyListener();
        hoursfrom.setKeyListener(null);
        hoursfrom.setTypeface(CustomFonts.getTypeface(this,CustomFonts.PROXIMA_NOVA_REGULAR));
        hoursfrom.setOnClickListener(this);
        hoursto = (EditText)findViewById(R.id.sethourto);
        kl5 = hoursto.getKeyListener();
        hoursto.setKeyListener(null);
        hoursto.setTypeface(CustomFonts.getTypeface(this,CustomFonts.PROXIMA_NOVA_REGULAR));
        hoursto.setOnClickListener(this);
        employees = (EditText)findViewById(R.id.setempno);
        kl6 = employees.getKeyListener();
        employees.setKeyListener(null);
        employees.setTypeface(CustomFonts.getTypeface(this,CustomFonts.PROXIMA_NOVA_REGULAR));


        /* Take the references of the Titles(Sub-catogory) and set the font style as PROXIMA_NOVA_BOLD*/
        bnameTitle = (TextView)findViewById(R.id.bnametitle);
        bnameTitle.setTypeface(CustomFonts.getTypeface(this, CustomFonts.PROXIMA_NOVA_SBOLD));
        contactTitle = (TextView)findViewById(R.id.contacttitle);
        contactTitle.setTypeface(CustomFonts.getTypeface(this, CustomFonts.PROXIMA_NOVA_SBOLD));
        workTitle = (TextView)findViewById(R.id.worktitle);
        workTitle.setTypeface(CustomFonts.getTypeface(this, CustomFonts.PROXIMA_NOVA_SBOLD));

        ab = new AlertDialog.Builder(this);
        ab.setTitle("Error!!");
        ab.setMessage("Please sign in to view account details and settings.");
        ab.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                finish();
            }
        });
        ab.setPositiveButton("SignIn", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(SettingsActivity.this, SignInActivity.class));
            }
        });

        if(AppPreferences.getAccessToken(this)!=null){
            setDetails();
        }
        else
            ab.show();


        //Set click listener to the edit buttons in-order to make the the corresponding fields editable

        editBname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bname.setKeyListener(kl1);
            }
        });
        editEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email.setKeyListener(kl2);
            }
        });
        editOperationaHour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hoursfrom.setKeyListener(kl4);
                hoursto.setKeyListener(kl5);
            }
        });
        editEmployeeNO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                employees.setKeyListener(kl6);
            }
        });

    }

    private void setDetails() {
        bname.setText(AppPreferences.getBusinessName(this));
        email.setText(AppPreferences.getEmailId(this));
        phone.setText(AppPreferences.getPhoneNo(this));
        hoursfrom.setText(AppPreferences.getOperationalHourFrom(this));
        hoursto.setText(AppPreferences.getOperationalHourTo(this));
    }

    public void back(View view){
        finish();
        startActivity(new Intent(SettingsActivity.this, HomeActivity.class));
    }

    public void logout(View view){
        AppPreferences.setAccessToken(this, null);
        Toast.makeText(this,"Successfully Logged out!",Toast.LENGTH_SHORT).show();
        finish();
    }

    public void updateProfile(View view){
        if(bname.getText().toString().trim().isEmpty()){
            AlertDialog.Builder ab = new AlertDialog.Builder(SettingsActivity.this);
            ab.setTitle("Error!!!");
            ab.setMessage("Please Enter a valid business name");
            ab.setCancelable(false);
            ab.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            ab.show();
        }
        else if (email.getText().toString().trim().isEmpty()||!email.getText().toString().trim().matches(EMAIL_PATTERN)) {
            AlertDialog.Builder ab = new AlertDialog.Builder(SettingsActivity.this);
            ab.setTitle("Error!!!");
            ab.setMessage("Please Enter a valid Email address");
            ab.setCancelable(false);
            ab.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            ab.show();
        }
        else if (isNetworkAvailable()) {
        String tag_json_obj = "json_obj_update";

        String url = CreateUrl.getSettingsUrl();

        Map<String, String> params = new HashMap<>();


            final ProgressDialog progessDialog = new ProgressDialog(this);
            progessDialog.setMessage("Updating...");
            progessDialog.show();

//        params.put("token",AppPreferences.getAccessToken(this));
        params.put("merchantId",AppPreferences.getMerchantId(this));
        params.put("businessname",bname.getText().toString().trim());
        params.put("mobile",phone.getText().toString().trim());
        params.put("email", email.getText().toString().trim());
        params.put("openTime", hoursfrom.getText().toString().trim());
        params.put("closeTime", hoursto.getText().toString().trim());
        params.put("noofemployees", employees.getText().toString().trim());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try{
                    Log.i("Rahul", AppPreferences.getAccessToken(SettingsActivity.this).toString());
                    Log.i("Rahul", response.toString());
                    if (response.getBoolean("success")) {
                        JSONObject user = response.getJSONObject("doc");
                        AppPreferences.setBusinessName(SettingsActivity.this, user.getString("businessname"));
                        AppPreferences.setEmailId(SettingsActivity.this, user.getString("email"));
                        AppPreferences.setPhoneNo(SettingsActivity.this, user.getString("mobile"));
                        AppPreferences.setOperationalHourFrom(SettingsActivity.this, user.getString("openTime"));
                        AppPreferences.setOperationalHourTo(SettingsActivity.this,user.getString("closeTime"));
//                        AppPreferences.setOperationalHourTo(SettingsActivity.this,user.getString("noofemployees"));
                        displayToast("Sucessfully Updated");
                        SettingsActivity.this.finish();
                        startActivity(getIntent());
                    } else {
                        Toast.makeText(SettingsActivity.this,"There was an error/please fill the fields correctly",Toast.LENGTH_SHORT).show();
                    }

                }catch (Exception e){
                    displayToast(e.getMessage());
                }
                progessDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Rahul ", error.toString());
                progessDialog.dismiss();
                displayToast(error.getMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("merchantId",AppPreferences.getMerchantId(SettingsActivity.this));
                params.put("businessname",bname.getText().toString().trim());
                params.put("mobile",phone.getText().toString().trim());
                params.put("email", email.getText().toString().trim());
                params.put("openTime", hoursfrom.getText().toString().trim());
                params.put("closeTime", hoursto.getText().toString().trim());
                params.put("noofemployees", employees.getText().toString().trim());
                return params;
            }

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", AppPreferences.getAccessToken(SettingsActivity.this));
                return headers;
            }
        };

        AircountrApplication.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_obj);
        } else {
            showDialog("Error", getResources().getString(R.string.no_internet), "OK");
        }

    }

    private void timePickerDialog() {
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                SettingsActivity.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.setThemeDark(false);
        tpd.vibrate(false);
        tpd.setCancelable(false);
        tpd.dismissOnPause(true);
        tpd.enableSeconds(false);
        tpd.setAccentColor(getResources().getColor(R.color.theme_color_blue));
        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d("TimePicker", "Dialog was cancelled");
            }
        });
        tpd.show(getFragmentManager(), "Timepickerdialog");
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        if (BTN_TYPE == 1)
            updateTime(hourOfDay, minute, hoursfrom);
        else if (BTN_TYPE == 2)
            updateTime(hourOfDay, minute, hoursto);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.sethourfrom:
                BTN_TYPE = 1;
                timePickerDialog();
                break;
            case R.id.sethourto:
                BTN_TYPE = 2;
                timePickerDialog();
                break;
        }
    }

    private void updateTime(int hours, int minute, EditText timeText) {
        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";

        String minutes = "";
        if (minute < 10)
            minutes = "0" + minute;
        else
            minutes = String.valueOf(minute);
        String aTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();
        timeText.setText(aTime);
        timeText.setTextColor(getResources().getColor(R.color.black));
    }
}
